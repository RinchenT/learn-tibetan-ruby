class ApplicationController < ActionController::Base
  helper_method :current_user

  def current_user
    User.find_by(id: session[:user_id])
  end

  def send_404
    redirect_to page_not_found_path
  end

end
