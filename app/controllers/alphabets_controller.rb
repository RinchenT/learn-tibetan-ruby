class AlphabetsController < ApplicationController
  def index
    @alphabets = Alphabet.all
  end

  def alphabet_params
    params.require(:alphabets).permit(:answer)
  end
end
