class AlphabetAnswersController < ApplicationController
  before_action :initialize_data, only: [:show]
  include AlphabetAnswersHelper

  def index
  end

  def show
  end

  def update
    @alphabet_answer = AlphabetAnswer.find(params[:id])

    @alphabet_answer.update(alphabet_answers_params)
    @alphabet_answer.correct = @alphabet_answer.alphabet.alphabet.upcase === @alphabet_answer.answer.upcase

    respond_to do |format|
      if @alphabet_answer.save
        if @alphabet_answer.correct
          if next_page(@alphabet_answer.alphabet.rank)
            format.html { redirect_to alphabet_answer_path(@alphabet_answer.alphabet.rank+1) }
            format.json { render :show, status: :created, location: alphabet_answer_path(@alphabet_answer.alphabet.rank+1) }
          else
            format.html { redirect_to alphabet_answers_finished_path }
            format.json { render :show, status: :created, location: alphabet_answers_finished_path }
          end
        else
          format.html { redirect_to alphabet_answer_path(@alphabet_answer.alphabet.rank), notice: 'Incorrect answer' }
        end
      end
    end
  end

  def restart
    current_user.alphabet_answers.destroy_all
    redirect_to alphabet_answers_path
  end

  def begin
    unless current_user.alphabet_answers.any?
      Alphabet.all.each do |alphabet|
        AlphabetAnswer.create(user_id: current_user.id, alphabet_id: alphabet.rank)
      end
    end

    redirect_to alphabet_answer_path(1)
  end

  def initialize_data
    @alphabet = Alphabet.find_by(rank: params[:alphabet_id])
    @alphabet_answer = AlphabetAnswer.find_by(user_id: current_user.id, alphabet_id: @alphabet.id) if @alphabet.present?
    send_404 unless @alphabet.present?
  end

  def alphabet_answers_params
    params.require(:alphabet_answer).permit(:answer, :alphabet_id)
  end

end
