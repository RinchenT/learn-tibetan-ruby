class User < ApplicationRecord
  has_secure_password

  validates :email, uniqueness: true, presence: true

  has_many :alphabet_answers

  def unanswered_questions
    alphabet_answers.where(answer: ["", nil])
  end
end
