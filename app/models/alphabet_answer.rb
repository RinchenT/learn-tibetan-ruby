class AlphabetAnswer < ApplicationRecord
  belongs_to :alphabet
  belongs_to :user

  scope :correct_answers, -> { where(correct: true) }
end
