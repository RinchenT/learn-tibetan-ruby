module AlphabetAnswersHelper
  def next_page(rank)
    Alphabet.where(rank: rank+1)&.first&.rank
  end

  def previous_page(rank)
    Alphabet.where(rank: rank-1)&.first&.rank
  end
end
