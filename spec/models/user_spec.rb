require 'rails_helper'

RSpec.describe User, type: :model do
  it "should only allow a non-existing email to be created" do
    create(:user)
  end

  it "should not allow an existing email to be created" do
    create(:user)

    expect { create(:user) }.to raise_error
  end
end
