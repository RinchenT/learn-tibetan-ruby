class CreateAlphabetsTable < ActiveRecord::Migration[5.2]
  def change
    create_table :alphabets do |t|
      t.string :alphabet
      t.string :image_url
    end
  end
end
