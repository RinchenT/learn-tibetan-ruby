class RemoveAlphabetsTable < ActiveRecord::Migration[5.2]
  def change
    drop_table :alphabets
  end
end
