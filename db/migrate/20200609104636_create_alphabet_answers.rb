class CreateAlphabetAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :alphabet_answers do |t|
      t.integer :alphabet_id
      t.string :answer
      t.boolean :correct
      t.integer :user_id
      t.timestamps
    end
  end
end
