Rails.application.routes.draw do

  get 'error/index'
  get 'sessions/new'
  get 'sessions/create'
  get 'sessions/login'
  root :to => 'home#index'

  get 'home', to: 'home#index'
  get 'signup', to: 'users#new'
  get 'navbar', to: 'navbar#index'
  get 'study', to: 'study#home'

  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'

  get 'alphabet_answers/finished'
  get 'alphabet_answers/restart', to: 'alphabet_answers#restart'
  get 'alphabet_answers/begin', to: 'alphabet_answers#begin'
  
  get 'alphabet_answers/:alphabet_id', to: 'alphabet_answers#show'

  get 'page_not_found', to: 'error#show'

  resources :users
  resources :study
  resources :alphabets
  resources :alphabet_answers
end
